﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;


namespace proyecto_mesaAyuda7.Controllers
{
    public class Empleados7Controller : ApiController
    {
        string cadenaConexion = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\BDmesaAyuda7.mdf;Integrated Security=True";
        //Data Source = (LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\BDMESA_AYUDA.mdf;Integrated Security = True
        // GET: api/Clientes
        public IEnumerable<Models.Empleado> Get()//Retorna un listado de la interfaz IEnumerable
        {
            DataSet objDataSet = null;//Guarda el resultado de la consulta

            IEnumerable<Models.Empleado> listadoEmpleados = null;
            string consultaSql = "SELECT * FROM EMPLEADO";
            ControlConexion objControlConexion = new ControlConexion(cadenaConexion);
            objControlConexion.abrirBD();
            objDataSet = objControlConexion.ejecutarConsultasSql(consultaSql);
            var myData = objDataSet.Tables[0].AsEnumerable().Select(//var es para a una variable cualquiera asignarle el tipo que sea 
                r => new Models.Empleado//Instancia la clase empleado
                {
                    IdEmple = r.Field<string>("IDEMPLEADO"),
                    Nombre = r.Field<string>("NOMBRE"),
                    Foto = r.Field<string>("FOTO"),
                    HojaVida = r.Field<string>("HOJAVIDA"),
                    Tel = r.Field<string>("TELEFONO"),
                    Dir = r.Field<string>("DIRECCION"),
                    Ema = r.Field<string>("EMAIL"),
                    X = r.Field<double>("X"),
                    Y = r.Field<double>("Y"),
                    FkEmple_Jefe = r.Field<string>("FKEMPLE_JEFE"),
                    FkArea = r.Field<string>("FKAREA")
                });
            listadoEmpleados = myData.ToList();
            return listadoEmpleados;
        }

        // GET: api/Empleado1/5
        public IEnumerable<Models.Empleado> Get(string id)//Retorna un listado de la interfaz IEnumerable
        {
            DataSet objDataSet = null;//Guarda el resultado de la consulta

            IEnumerable<Models.Empleado> listadoEmpleados = null;
            string consultaSql = "SELECT * FROM EMPLEADO WHERE IDEMPLEADO='" + id + "'";
            ControlConexion objControlConexion = new ControlConexion(cadenaConexion);
            objControlConexion.abrirBD();
            objDataSet = objControlConexion.ejecutarConsultasSql(consultaSql);
            var myData = objDataSet.Tables[0].AsEnumerable().Select(//var es para a una variable cualquiera asignarle el tipo que sea 
                r => new Models.Empleado//Instancia la clase empleado
                {
                    IdEmple = r.Field<string>("IDEMPLEADO"),
                    Nombre = r.Field<string>("NOMBRE"),
                    Foto = r.Field<string>("FOTO"),
                    HojaVida = r.Field<string>("HOJAVIDA"),
                    Tel = r.Field<string>("TELEFONO"),
                    Dir = r.Field<string>("DIRECCION"),
                    Ema = r.Field<string>("EMAIL"),
                    X = r.Field<double>("X"),
                    Y = r.Field<double>("Y"),
                    FkEmple_Jefe = r.Field<string>("FKEMPLE_JEFE"),
                    FkArea = r.Field<string>("FKAREA")

                });
            listadoEmpleados = myData.ToList();
            return listadoEmpleados;
        }

        // POST: api/Empleado1
        public IHttpActionResult Post(Models.Empleado objEmpleado)
        {
            string comandoSql = String.Format("INSERT INTO EMPLEADO(IDEMPLEADO, NOMBRE,FOTO,HOJAVIDA,TELEFONO,EMAIL,DIRECCION,X,Y,FKEMPLE_JEFE,FKAREA) VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}',{10})", objEmpleado.IdEmple, objEmpleado.Nombre, objEmpleado.Foto, objEmpleado.HojaVida, objEmpleado.Tel, objEmpleado.Ema, objEmpleado.Dir, objEmpleado.X, objEmpleado.Y, objEmpleado.FkEmple_Jefe, objEmpleado.FkArea);
            ControlConexion objControlConexion = new ControlConexion(cadenaConexion);
            objControlConexion.abrirBD();
            string msg = objControlConexion.ejecutarComandoSQL(comandoSql);
            if (msg == "ok") return Content(HttpStatusCode.Created, "Transacción exitosa");
            else return Content(HttpStatusCode.Conflict, "No se pudo guardar el registro");
        }

        // PUT: api/Empleado1/5
        public IHttpActionResult Put(Models.Empleado objEmpleado)
        {
            string comandoSql = String.Format("UPDATE EMPLEADO SET NOMBRE='{0}',FOTO='{1}',HOJAVIDA='{2}',TELEFONO='{3}',EMAIL='{4}',DIRECCION='{5}',X={6},Y={7},FKEMPLE_JEFE='{8}',FKAREA='{9}' WHERE IDEMPLEADO='{10}'", objEmpleado.Nombre, objEmpleado.Foto, objEmpleado.HojaVida, objEmpleado.Tel, objEmpleado.Ema, objEmpleado.Dir, objEmpleado.X, objEmpleado.Y, objEmpleado.FkEmple_Jefe, objEmpleado.FkArea, objEmpleado.IdEmple);
            ControlConexion objControlConexion = new ControlConexion(cadenaConexion);
            objControlConexion.abrirBD();
            string msg = objControlConexion.ejecutarComandoSQL(comandoSql);
            if (msg == "ok") return Content(HttpStatusCode.Created, "Transacción exitosa");
            else return Content(HttpStatusCode.Conflict, "No se pudo Modificar el registro");
        }

        // DELETE: api/Empleado1/5
        public IHttpActionResult Delete(Models.Empleado objEmpleado)
        {
            string comandoSql = String.Format("DELETE FROM EMPLEADO WHERE IDEMPLEADO='{0}'", objEmpleado.IdEmple);
            ControlConexion objControlConexion = new ControlConexion(cadenaConexion);
            objControlConexion.abrirBD();
            string msg = objControlConexion.ejecutarComandoSQL(comandoSql);
            if (msg == "ok") return Content(HttpStatusCode.Created, "Transacción exitosa");
            else return Content(HttpStatusCode.Conflict, "No se pudo Modificar el registro");
        }
    }
}
