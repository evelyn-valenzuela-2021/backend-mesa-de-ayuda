﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;

namespace proyecto_mesaAyuda7.Controllers
{
    public class DetalleReq1Controller : ApiController
    {
        string cadenaConexion = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\familiaCL\source\repos\proyecto_mesaAyuda7\proyecto_mesaAyuda7\App_Data\BDmesaAyuda7.mdf;Integrated Security=True";

        // GET: api/Clientes
        public IEnumerable<Models.DetalleReq1> Get()//Retorna un listado de la interfaz IEnumerable
        {
            DataSet objDataSet = null;//Guarda el resultado de la consulta

            IEnumerable<Models.DetalleReq1> listadoDetalleReq = null;
            string consultaSql = "SELECT * FROM DETALLEREQ";
            ControlConexion objControlConexion = new ControlConexion(cadenaConexion);
            objControlConexion.abrirBD();
            objDataSet = objControlConexion.ejecutarConsultasSql(consultaSql);
            var myData = objDataSet.Tables[0].AsEnumerable().Select(//var es para a una variable cualquiera asignarle el tipo que sea 
                r => new Models.DetalleReq1//Instancia la clase empleado
                {
                    Id = r.Field<int>("IDDETALLE"),
                    Fecha = r.Field<DateTime>("FECHA"),
                    Observacion = r.Field<string>("OBSERVACION"),
                    FkReq = r.Field<int>("FKREQ"),
                    FkEstado = r.Field<string>("FKESTADO"),
                    FkEmple = r.Field<string>("FKEMPLE"),
                    FkEmpleAsignado = r.Field<string>("FKEMPLEASIGNADO")


                });
            listadoDetalleReq = myData.ToList();
            return listadoDetalleReq;
        }

        // GET: api/DetalleReq1/5
        public IEnumerable<Models.DetalleReq1> Get(int id)//Retorna un listado de la interfaz IEnumerable
        {
            DataSet objDataSet = null;//Guarda el resultado de la consulta

            IEnumerable<Models.DetalleReq1> listadoDetalleReq = null;
            string consultaSql = "SELECT * FROM DETALLEREQ WHERE IDDETALLE = " + id;
            ControlConexion objControlConexion = new ControlConexion(cadenaConexion);
            objControlConexion.abrirBD();
            objDataSet = objControlConexion.ejecutarConsultasSql(consultaSql);
            var myData = objDataSet.Tables[0].AsEnumerable().Select(//var es para a una variable cualquiera asignarle el tipo que sea 
                r => new Models.DetalleReq1//Instancia la clase empleado
                {
                    Id = r.Field<int>("IDDETALLE"),
                    Fecha = r.Field<DateTime>("FECHA"),
                    Observacion = r.Field<string>("OBSERVACION"),
                    FkReq = r.Field<int>("FKREQ"),
                    FkEstado = r.Field<string>("FKESTADO"),
                    FkEmple = r.Field<string>("FKEMPLE"),
                    FkEmpleAsignado = r.Field<string>("FKEMPLEASIGNADO")


                });
            listadoDetalleReq = myData.ToList();
            return listadoDetalleReq;
        }
    }
}
