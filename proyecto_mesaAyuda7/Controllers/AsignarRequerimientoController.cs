﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;

namespace proyecto_mesaAyuda7.Controllers
{

    public class AsignarRequerimientoController : ApiController
    {

        string cadenaConexion = @"Data Source = (LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\BDMESA_AYUDA.mdf;Integrated Security = True";

        // GET: api/AsignarRequerimiento
        public IEnumerable<string> Get()
        {
            return new string[] { "debe seleccionar Post" };
        }

        // GET: api/AsignarRequerimiento/5
        public string Get(int id)
        {
            return "debe seleccionar Post";
        }

        // POST: api/AsignarRequerimiento
        public IHttpActionResult Post(Models.DetalleReq1 objDetalleReq1)
        {
            string comandoSql = String.Format("EXEC PA_ASIGNAR_REQUERIMIENTO  @OBSERVACION = '{0}', @FKEMPLE = '{1}', @FKEMPLEASIGNADO= '{2}', @FKREQ={3}", objDetalleReq1.Observacion, objDetalleReq1.FkEmple, objDetalleReq1.FkEmpleAsignado, objDetalleReq1.FkReq);
            ControlConexion objControlConexion = new ControlConexion(cadenaConexion);
            objControlConexion.abrirBD();
            string msg = objControlConexion.ejecutarComandoSQL(comandoSql);
            if (msg == "ok") return Content(HttpStatusCode.Created, "Transacción exitosa");
            else return Content(HttpStatusCode.Conflict, "No se pudo guardar el registro");
        }
    }
}
