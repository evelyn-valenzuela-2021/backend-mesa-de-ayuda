﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace proyecto_mesaAyuda7.Controllers
{
    public class RadicarRequerimientoController : ApiController
    {
        string cadenaConexion = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\familiaCL\source\repos\proyecto_mesaAyuda7\proyecto_mesaAyuda7\App_Data\BDmesaAyuda7.mdf;Integrated Security=True";

        // GET: api/RadicarRequerimiento
        public IEnumerable<string> Get()
        {
            return new string[] { "debe seleccionar Post" };
        }

        // GET: api/RadicarRequerimiento/5
        public string Get(int id)
        {
            return "debe seleccionar Post";
        }

        // POST: api/RadicarRequerimiento
        public IHttpActionResult Post(Models.DetalleReq1 objDetalleReq1)
        {
            string area = "10";//esto hay que mejorarlo
            string comandoSql = String.Format("EXEC PA_RADICAR_REQUERIMIENTO @FKAREA ='{0}', @OBSERVACION = '{1}', @FKEMPLE = '{2}'", area, objDetalleReq1.Observacion, objDetalleReq1.FkEmple);
            ControlConexion objControlConexion = new ControlConexion(cadenaConexion);
            objControlConexion.abrirBD();
            string msg = objControlConexion.ejecutarComandoSQL(comandoSql);
            if (msg == "ok") return Content(HttpStatusCode.Created, "Transacción exitosa");
            else return Content(HttpStatusCode.Conflict, "No se pudo radicar el registro");
        }
    }
}
