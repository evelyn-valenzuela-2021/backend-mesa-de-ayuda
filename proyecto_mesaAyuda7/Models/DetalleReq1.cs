﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace proyecto_mesaAyuda7.Models
{
    public class DetalleReq1
    {
        private int id;
        private DateTime fecha;
        private string observacion;
        private int fkReq;
        private string fkEstado;
        private string fkEmple;
        private string fkEmpleAsignado;

        public int Id { get => id; set => id = value; }
        public DateTime Fecha { get => fecha; set => fecha = value; }
        public string Observacion { get => observacion; set => observacion = value; }
        public int FkReq { get => fkReq; set => fkReq = value; }
        public string FkEstado { get => fkEstado; set => fkEstado = value; }
        public string FkEmple { get => fkEmple; set => fkEmple = value; }
        public string FkEmpleAsignado { get => fkEmpleAsignado; set => fkEmpleAsignado = value; }
    }
}