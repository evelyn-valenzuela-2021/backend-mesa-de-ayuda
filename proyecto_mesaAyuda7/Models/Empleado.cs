﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace proyecto_mesaAyuda7.Models
{
    public class Empleado
    {
        private string idEmple;
        private string nombre;
        private string foto;
        private string hojaVida;
        private string tel;
        private string ema;
        private string dir;
        private double x;
        private double y;
        private string fkEmple_Jefe;
        private string fkArea;


        public string IdEmple { get => idEmple; set => idEmple = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Foto { get => foto; set => foto = value; }
        public string HojaVida { get => hojaVida; set => hojaVida = value; }
        public string Tel { get => tel; set => tel = value; }
        public string Ema { get => ema; set => ema = value; }
        public string Dir { get => dir; set => dir = value; }
        public double X { get => x; set => x = value; }
        public double Y { get => y; set => y = value; }
        public string FkEmple_Jefe { get => fkEmple_Jefe; set => fkEmple_Jefe = value; }
        public string FkArea { get => fkArea; set => fkArea = value; }
    }
}