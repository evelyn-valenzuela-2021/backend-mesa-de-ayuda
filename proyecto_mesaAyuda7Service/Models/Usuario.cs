﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace proyecto_mesaAyuda7Service.Models {
    public class Usuario {

        private int idUsuario;
        private string correo;
        private string contrasena;
        private string fk_Empleado;

        
        public string Correo { get => correo; set => correo = value; }
        public string Contrasena { get => contrasena; set => contrasena = value; }
        public int IdUsuario { get => idUsuario; set => idUsuario = value; }
        public string Fk_Empleado { get => fk_Empleado; set => fk_Empleado = value; }
    }
}