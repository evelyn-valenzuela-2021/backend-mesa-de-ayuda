﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace proyecto_mesaAyuda7Service.Models {

    public class Area {

        private string idArea;
        private string nombre;
        private string fkEmple;

        public string IdArea { get => idArea; set => idArea = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string FkEmple { get => fkEmple; set => fkEmple = value; }
    }
}