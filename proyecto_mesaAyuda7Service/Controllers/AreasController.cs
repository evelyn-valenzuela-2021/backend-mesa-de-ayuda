﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace proyecto_mesaAyuda7Service.Controllers
{
    public class AreasController : ApiController
    {
        // GET api/areas
        public IEnumerable<Models.Area> Get()
        {
            string cadenaConexion = @"Data Source = (LocalDB)\MSSQLLocalDB;AttachDbFilename=E:\Universidad\S7\VV de software\proyecto_mesaAyuda7\proyecto_mesaAyuda7\proyecto_mesaAyuda7\App_Data\BDmesaAyuda7.mdf;Integrated Security = True";

            DataSet objDataSet = null;
            IEnumerable<Models.Area> listadoAreas = null;

            string consultaSql = "SELECT * FROM AREA";
            ControlConexion objControlConexion = new ControlConexion(cadenaConexion);
            objControlConexion.abrirBD();
            objDataSet = objControlConexion.ejecutarConsultasSql(consultaSql);
            var myData = objDataSet.Tables[0].AsEnumerable().Select(
                r => new Models.Area
                {
                    IdArea = r.Field<string>("IDAREA"),
                    Nombre = r.Field<string>("NOMBRE"),
                    FkEmple = r.Field<string>("FKEMPLE")
                });
            listadoAreas = myData.ToList();
            return listadoAreas;
        }

        // GET api/areas
        public IEnumerable<Models.Area> Get(string id)
        {
            string cadenaConexion = @"Data Source = (LocalDB)\MSSQLLocalDB;AttachDbFilename=E:\Universidad\S7\VV de software\proyecto_mesaAyuda7\proyecto_mesaAyuda7\proyecto_mesaAyuda7\App_Data\BDmesaAyuda7.mdf;Integrated Security = True";

            DataSet objDataSet = null;
            IEnumerable<Models.Area> listadoAreas = null;

            string consultaSql = "SELECT * FROM AREA WHERE IDAREA= '" + id + "'";
            ControlConexion objControlConexion = new ControlConexion(cadenaConexion);
            objControlConexion.abrirBD();
            objDataSet = objControlConexion.ejecutarConsultasSql(consultaSql);
            var myData = objDataSet.Tables[0].AsEnumerable().Select(
                r => new Models.Area
                {
                    IdArea = r.Field<string>("IDAREA"),
                    Nombre = r.Field<string>("NOMBRE"),
                    FkEmple = r.Field<string>("FKEMPLE")
                });
            listadoAreas = myData.ToList();
            return listadoAreas;
        }

    }
}