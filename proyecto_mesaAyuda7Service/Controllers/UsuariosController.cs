﻿using proyecto_mesaAyuda7Service.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace proyecto_mesaAyuda7Service.Controllers {

    public class UsuariosController : ApiController {
       
        // POST api/usuarios
        public IEnumerable<Usuario> Post(Usuario usuario) {

            DataSet objDataSet = null;
            string cadenaConexion = @"Data Source = (LocalDB)\MSSQLLocalDB;AttachDbFilename=E:\Universidad\S7\VV de software\proyecto_mesaAyuda7\proyecto_mesaAyuda7\proyecto_mesaAyuda7\App_Data\BDmesaAyuda7.mdf;Integrated Security = True";

            string consultaSql = "SELECT * FROM USUARIO WHERE CORREO='" + usuario.Correo + "'" + " AND CONTRASENA= '" + usuario.Contrasena + "'";
            ControlConexion objControlConexion = new ControlConexion(cadenaConexion);
            objControlConexion.abrirBD();
            objDataSet = objControlConexion.ejecutarConsultasSql(consultaSql);
            var myData = objDataSet.Tables[0].AsEnumerable().Select(
                r => new Models.Usuario
                {
                    IdUsuario = r.Field<int>("IDUSUARIO"),
                    Correo = r.Field<string>("CORREO"),
                    Contrasena = r.Field<string>("CONTRASENA"),
                    Fk_Empleado = r.Field<string>("FK_EMPLEADO")

                });
            return myData.ToList();
        }
    }
}